openssl req -newkey rsa:4096 \
            -x509 \
            -sha256 \
            -days 3650 \
            -nodes \
            -out ./config/example.crt \
            -keyout ./config/example.key \
            -subj "/C=SI/ST=Test/L=Test/O=Security/OU=IT_Department/CN=www.example.com"
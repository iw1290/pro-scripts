#!/bin/bash
work_path=$(dirname $(readlink -f $0))

echo $work_path
cd $work_path

nginx_port=$(shuf -i 2000-65000 -n 1)
trojan_port=$(shuf -i 2000-65000 -n 1)
password=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 32 ; echo '')

echo "nginx_port: $nginx_port" 
echo "trojan_port: $trojan_port"
echo "password: $password"

./create-cert-auto.sh

sed -i "s/18080/$nginx_port/" trojan-nginx
sed -i "s/18080/$nginx_port/" ./config/config.json
sed -i "s/18081/$trojan_port/" ./config/config.json
sed -i "s/__REPLACE_ME_THIS_IS_PASSWORD__/$password/" ./config/config.json

ln -s $work_path/trojan-nginx /etc/nginx/sites-enabled/trojan-nginx
nginx -s reload

./init.sh

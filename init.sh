docker rm -f trojan
docker run -d \
    --name trojan \
    --restart=always \
    --net=host \
    -v $PWD/config:/config \
    trojangfw/trojan

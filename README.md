# Tested in Ubuntu 20.04

```sh
apt install docker.io nginx -y
# disable firewall
ufw disable

mkdir -p /usr/server
git clone https://gitlab.com/iw1290/pro-scripts.git /usr/server/trojan
/usr/server/trojan/autorun.sh
```